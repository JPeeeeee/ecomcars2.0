import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
    modeloText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light',
        fontSize: 25
    },

    tituloText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light',
        fontSize: 17
    },

    buttonText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light'
    },

    circle: {
        marginTop:"35%",
        marginLeft:"20%",
        backgroundColor: '#FFFFFF',
        borderRadius: 100,
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
}) 

export default styles