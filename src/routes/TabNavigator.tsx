import React from 'react';
import { View } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeScreen from '../screens/HomeScreen/HomeScreen'
import CarsScreen from '../screens/CarsScreen/CarsScreen';
import ProfileScreen from '../screens/ProfileScreen/ProfileScreen';

const Tab = createBottomTabNavigator();

export default function Navigation() {;
    return (
        <Tab.Navigator screenOptions={ ({route}) => ({ 
            tabBarStyle: { 
                position: 'absolute',
                backgroundColor: '#FFFFFF',
                borderRadius: 50,
            }, 
            tabBarIcon: ({ color, focused }) => {
                if (route.name === 'Home') {
                    color = focused ? '#A07A28' : '#8A8A8A'
                    return (
                        <View style={{ justifyContent: 'center', flex: 1}}>
                            <MaterialCommunityIcons
                                name="home"
                                size={40}
                                color={color}
                            />
                        </View>
                    )
                }
                else if (route.name === 'Cars') {
                    color = focused ? '#A07A28' : '#8A8A8A'
                    return (
                        <View style={{ justifyContent: 'center', flex: 1}}>
                            <MaterialCommunityIcons
                                name="car"
                                size={40}
                                color={color}
                            />
                        </View>
                    )
                }
                else if (route.name === 'Profile') {
                    color = focused ? '#A07A28' : '#8A8A8A'
                    return (
                        <View style={{ justifyContent: 'center', flex: 1}}>
                            <MaterialCommunityIcons
                                name="account"
                                size={40}
                                color={color}
                            />
                        </View>
                    )
                }
            },
            headerShown: false,
            tabBarShowLabel: false
        })}>

            <Tab.Screen name="Home" component={HomeScreen} />
            <Tab.Screen name="Cars" component={CarsScreen} />
            <Tab.Screen name="Profile" component={ProfileScreen} />
        </Tab.Navigator>
    )
}
