import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { NavigationContainer } from '@react-navigation/native'
import LoginScreen from '../screens/LoginScreen/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen/RegisterScreen';
import Navigation from './TabNavigator';
import SelectCarScreen from '../screens/SelectCarScreen/SelectCarScreen';



const Stack = createNativeStackNavigator();

export default function StackNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Login" component={LoginScreen} />
                <Stack.Screen name="Register" component={RegisterScreen} />
                <Stack.Screen name="Tab" component={Navigation} />
                <Stack.Screen name="SelectCar" component={SelectCarScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}