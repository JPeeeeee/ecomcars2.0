import React, { useState } from 'react'
import { Text, View, Image, StatusBar, TextInput, TouchableOpacity } from 'react-native'

import styles from './LoginScreenStyles'
import globalStyles from '../../../global/globalStyles';

export default function LoginScreen ({navigation}: any) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const OnPressedEntrar = () => {
        navigation.navigate('Tab')
    }
    
    const OnPressedRegistrar = () => {
        navigation.navigate('Register')
    }

    return (
        <View style={{ alignItems: 'center', flex: 1, backgroundColor: '#E5E5E5'}}>

            <StatusBar 
                translucent
                backgroundColor="transparent"
                barStyle="dark-content"
            />
            
            {/* Logo do Ecomcars */}
            <View style={{ marginTop: '50%' }}>
                <Image
                    source={require('../../../assets/ecomCars.png')}
                    alt="ecomCars"
                    resizeMode="contain"
                />
            </View>
            
            {/* Texto Entrar */}
            <View style={{ marginTop: '15%' }}>
                <Text style={styles.headerText}>Entrar</Text>
            </View>
            
            {/* Input do e-mail */}
            <View style={{ marginTop: '10%'}}>
                <TextInput 
                    style={globalStyles.input}
                    placeholder="E-mail"
                    value={email}
                    onChangeText={text => setEmail(text)}
                />
            </View>

            {/* Input da senha */}
            <View style={{ marginTop: '5%' }}>
                <TextInput 
                    style={globalStyles.input}
                    placeholder="Senha"
                    secureTextEntry={true}
                    value={password}
                    onChangeText={text => setPassword(text)}
                />
            </View>
            
            {/* Botão de entrar */}
            <TouchableOpacity onPress={() => OnPressedEntrar()}>
                <View style={{ marginTop: '5%'}}>
                    <View style={styles.button}>
                        <View style={{ alignItems: 'center', justifyContent: 'center',flex: 1}}>
                            <Text style={styles.buttonText}>Entrar no ecomCars+</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>

            {/* Botão de Registrar */}
            <TouchableOpacity onPress={() => OnPressedRegistrar()}>
                <View style={{ marginTop: '10%'}}>
                    <View style={styles.button}>
                        <View style={{ alignItems: 'center', justifyContent: 'center',flex: 1}}>
                            <Text style={styles.buttonText}>Registrar-me</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>

        </View>
    )
}