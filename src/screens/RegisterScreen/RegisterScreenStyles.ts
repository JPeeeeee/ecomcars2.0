import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    headerText: {
        color: '#A07A28',
        fontFamily: 'Poppins_300Light',
        fontSize: 20,
    },

    buttonText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light',
        fontSize: 20
    },

    button: {
        marginTop: '10%', 
        width: 200,
        height: 50,
        backgroundColor: '#A07A28',
        borderRadius: 100,
    }
}) 

export default styles