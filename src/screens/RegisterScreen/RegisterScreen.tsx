import React, { useState } from 'react'
import { View, Text, Image, TextInput, StatusBar, TouchableOpacity } from 'react-native'

import globalStyles from '../../../global/globalStyles';
import styles from './RegisterScreenStyles'

export default function RegisterScreen ({navigation}: any) {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState ('');

    const OnPressedEntrar = () => {
        navigation.navigate('Tab')
    }

    return (
        <View style={{ alignItems: 'center', flex: 1, backgroundColor: '#E5E5E5'}}>

            <StatusBar 
                translucent
                backgroundColor={'transparent'}
                barStyle="dark-content"
            />

            {/* Logo do ecomCars */}
            <View style={{ marginTop: '40%' }}>
                <Image
                    source={require('../../../assets/ecomCars.png')}
                    alt="ecomCars"
                    resizeMode="contain"
                />
            </View>

            {/* Texto Entrar */}
            <View style={{ marginTop: '15%' }}>
                <Text style={styles.headerText}>Registrar</Text>
            </View>

            {/* Input do Nome */}
            <View style={{ marginTop: '10%'}}>
                <TextInput 
                    style={globalStyles.input}
                    placeholder="Nome"
                    value={name}
                    onChangeText={text => setName(text)}
                />
            </View>

            {/* Input do Email */}
            <View style={{ marginTop: '5%'}}>
                <TextInput 
                    style={globalStyles.input}
                    placeholder="E-mail"
                    value={email}
                    onChangeText={text => setEmail(text)}
                />
            </View>

            {/* Input da Senha */}
            <View style={{ marginTop: '5%'}}>
                <TextInput 
                    style={globalStyles.input}
                    placeholder="Senha"
                    value={password}
                    onChangeText={text => setPassword(text)}
                />
            </View>

            {/* Botão de entrar */}
            <TouchableOpacity onPress={() => OnPressedEntrar()}>
                <View style={styles.button}>
                    <View style={{ alignItems: 'center', justifyContent: 'center',flex: 1}}>
                        <Text style={styles.buttonText}>Entrar no ecomCars+</Text>
                    </View>
                </View>
            </TouchableOpacity>

        </View>
    )
}