import { TextStyle, TextStyleIOS, ViewStyle } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default {
    carBox: {
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        borderWidth: 3,
        borderColor: '#A07A28',
        borderTopColor: '#FFFFFF'
    } as ViewStyle,

    carName: {
        color: '#000000',
        fontFamily: 'Poppins_300Light',
        fontSize: 20
    } as TextStyle,

    footer: {
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 30,
        paddingVertical: 20,
        backgroundColor: '#FFFFFF',
        borderWidth: 2,
        borderColor: '#A07A28',
        flexDirection: 'row',
    } as ViewStyle, 

    specsCard: {
        alignItems: 'center',
        width: wp("40%"),
        paddingVertical: 5,
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
    } as ViewStyle,
    
    descricao: {
        width: wp("87%"),
        paddingVertical: 5,
        paddingHorizontal: 6,
        borderRadius: 20,
        backgroundColor: '#FFFFFF',
        marginBottom: "5%"
    } as ViewStyle,

    descricaoText: {
        color: '#000000',
        fontFamily: 'Poppins_300Light',
        fontSize: 15
    } as TextStyle,

    diariaText: {
        color: '#000000',
        fontFamily: 'Poppins_300Light',
        fontSize: 18
    } as TextStyle,

    reservar: {
        marginLeft: "35%",
        backgroundColor: '#3391AE',
        borderRadius: 20,
        height: hp("4%"),
        alignItems: 'center',
        justifyContent: 'center'
    } as ViewStyle,

    reservarText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light',
        fontSize: 20,
    } as TextStyle,
};
