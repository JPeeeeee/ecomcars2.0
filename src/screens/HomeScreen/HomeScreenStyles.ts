import { TextStyle } from "react-native";

export default {
    headerText: {
        color: '#000000',
        fontFamily: 'Poppins_300Light',
        marginTop: "10%",
        fontSize: 25
    } as TextStyle,

    buttonText: {
        color: '#FFFFFF',
        fontFamily: 'Poppins_300Light'
    } as TextStyle,
}