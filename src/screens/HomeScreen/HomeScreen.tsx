import { useQuery } from '@tanstack/react-query'
import { useAtom } from 'jotai'
import React, { useEffect, useState } from 'react'
import { View, Text, StatusBar, Image, FlatList, ActivityIndicator } from 'react-native'

import CarCard from '../../components/CarCard/CarCard'
import styles from './HomeScreenStyles'

async function getData() {
    const request = await fetch('https://run.mocky.io/v3/c4fd1252-5fec-40ef-9d15-f357dbda5d9a')
  
    const response = await request.json()
  
    return response.data
  }

export default function HomeScreen ({navigation}: any) {

    // const [atomData, setAtomData] = useAtom(atomData)

    // console.log('DADO FUNFOU:', data)

    const OnPressedCar = (item: any) => {
        navigation.navigate('SelectCar', { car : item })
    }

    // chama a api
    const { data, isLoading } = useQuery(["cars"], () => getData())

    if (isLoading){
        return <ActivityIndicator/>
    }

    return (

        <View style={{ alignItems: 'center', flex: 1,backgroundColor: "#E5E5E5" }}>

            <StatusBar 
                translucent
                backgroundColor={'transparent'}
                barStyle="dark-content"
            />
            
            {/* Logo do ecomCars */}
            <View style={{ marginTop: '15%' }}>
                <Image
                    source={require('../../../assets/ecomCars.png')}
                    alt={'ecomCars'}
                    resizeMode='contain'
                />
            </View>

            {/* Texto de destaque */}
            <Text style={styles.headerText}>Destaques para você</Text>

            {/* FlatLista dos Car Cars */}
            <FlatList
                showsVerticalScrollIndicator={false}
                data={data}
                keyExtractor={(item : any) => String(item.id)}
                renderItem={({ item } : any) => (
                <CarCard
                    modelo={item.modelo}
                    cor={item.colorhex}
                    titulo={item.titulo}
                    imagem={item.imagens[0]}
                    onPress={() => OnPressedCar(item)}
                />
                )}
            />

        </View>
    )
}