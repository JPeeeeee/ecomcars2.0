import { StyleSheet } from "react-native";

const globalStyles = StyleSheet.create({
    input: {
        width: 300, 
        height: 50,
        borderWidth: 2, 
        borderRadius: 100, 
        borderColor: '#A07A28',
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 20,
        fontSize: 20
    },
})

export default globalStyles